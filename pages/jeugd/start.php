	<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Jeugd</h1>
                <nav id="primary-menu">
						<ul>
                            <li>
								<a href="{url}jeugd/parfum">Parfum</a>
							</li>
							<li>
								<a href="{url}jeugd/make-up">Make-up</a>
							</li>
							<li>
								<a href="{url}jeugd/gezichtsverzorging">Gezichtsverzorging</a>
							</li>
							<li>
								<a href="{url}jeugd/lichaam-bad">Lichaam &amp; bad</a>
							</li>
                        </ul>
                    </nav>
            </div>

        </section>

        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="col_full">

							<div class="col_full portfolio-single-image masonry-thumbs col-3" data-big="3" data-lightbox="gallery" style="position: relative; height: 860px;">
								<a href="{url}images/mozaiek/jeugd-01.jpg" data-lightbox="gallery-item" style="width: 286px; position: absolute; left: 0px; top: 0px;"><img class="image_fade" src="{url}images/mozaiek/jeugd-01.jpg" alt="Gallery Thumb 1" style="opacity: 1;"></a>
								<a href="{url}images/mozaiek/jeugd-02.jpg" data-lightbox="gallery-item" style="width: 286px; position: absolute; left: 286px; top: 0px;"><img class="image_fade" src="{url}images/mozaiek/jeugd-02.jpg" alt="Gallery Thumb 2" style="opacity: 1;"></a>
								<a href="{url}images/mozaiek/jeugd-08.jpg" data-lightbox="gallery-item" style="width: 572px; position: absolute; left: 0px; top: 215px;"><img class="image_fade" src="{url}images/mozaiek/jeugd-08.jpg" alt="Gallery Thumb 3" style="opacity: 1;"></a>
								<a href="{url}images/mozaiek/jeugd-04.jpg" data-lightbox="gallery-item" style="width: 286px; position: absolute; left: 572px; top: 0px;"><img class="image_fade" src="{url}images/mozaiek/jeugd-04.jpg" alt="Gallery Thumb 4" style="opacity: 1;"></a>
								<a href="{url}images/mozaiek/jeugd-05.jpg" data-lightbox="gallery-item" style="width: 286px; position: absolute; left: 572px; top: 215px;"><img class="image_fade" src="{url}images/mozaiek/jeugd-05.jpg" alt="Gallery Thumb 5" style="opacity: 1;"></a>
								<a href="{url}images/mozaiek/jeugd-06.jpg" data-lightbox="gallery-item" style="width: 286px; position: absolute; left: 572px; top: 430px;"><img class="image_fade" src="{url}images/mozaiek/jeugd-06.jpg" alt="Gallery Thumb 6" style="opacity: 1;"></a>
								<a href="{url}images/mozaiek/jeugd-07.jpg" data-lightbox="gallery-item" style="width: 286px; position: absolute; left: 0px; top: 644px;"><img class="image_fade" src="{url}images/mozaiek/jeugd-07.jpg" alt="Gallery Thumb 7"></a>
								<a href="{url}images/mozaiek/jeugd-03.jpg" data-lightbox="gallery-item" style="width: 286px; position: absolute; left: 286px; top: 644px;"><img class="image_fade" src="{url}images/mozaiek/jeugd-03.jpg" alt="Gallery Thumb 8" style="opacity: 1;"></a>
								<a href="{url}images/mozaiek/jeugd-09.jpg" data-lightbox="gallery-item" style="width: 286px; position: absolute; left: 572px; top: 645px;"><img class="image_fade" src="{url}images/mozaiek/jeugd-09.jpg" alt="Gallery Thumb 9" style="opacity: 1;"></a>
							</div>
							
						</div>	
					</div>
					
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Recente artikels</h3>
								<ul>
									<?= Modules::run('posts/posts_logic/index', 'partial', 5, 'jeugd'); ?>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<ul>
									<li>
										<a href="{url}jeugd/parfum">Parfum</a>
									</li>
									<li>
										<a href="{url}jeugd/make-up">Make-up</a>
									</li>
									<li>
										<a href="{url}jeugd/gezichtsverzorging">Gezichtsverzorging</a>
									</li>
									<li>
										<a href="{url}jeugd/lichaam-bad">Lichaam &amp; bad</a>
									</li>
								</ul>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>