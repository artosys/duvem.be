		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Nieuws</h1>
                <nav id="primary-menu">
						<ul>
                            <li <?php if ( isset($_GET['cat']) && $_GET['cat'] == 'algemee' ) { echo 'class="current"'; } ?>>
								<a href="{url}nieuws?cat=algemee">Algemeen</a>
                            </li>
							<li <?php if ( isset($_GET['cat']) && $_GET['cat'] == 'vrouwen' ) { echo 'class="current"'; } ?>>
								<a href="{url}nieuws?cat=vrouwen">Vrouwen</a>
                            </li>
                            <li <?php if ( isset($_GET['cat']) && $_GET['cat'] == 'mannen' ) { echo 'class="current"'; } ?>>
                              <a href="{url}nieuws?cat=mannen">Mannen</a>
                            </li>
                            <li <?php if ( isset($_GET['cat']) && $_GET['cat'] == 'jeugd' ) { echo 'class="current"'; } ?>>
                              <a href="{url}nieuws?cat=jeugd">Jeugd</a>
                            </li>
                        </ul>
                    </nav>
            </div>

        </section>

		<section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="postcontent nobottommargin clearfix">
						
							<div id="posts" class="post-timeline clearfix">

								<div class="timeline-border"></div>
								<?php if ( isset($_GET['cat']) ) { $categorie = mysql_real_escape_string($_GET['cat']);  ?>
								<?= Modules::run('posts/posts_logic/index', 'partial', 10, $categorie); ?>
								<?php } else { ?>
								<?= Modules::run('posts/posts_logic/index', 'partial'); ?>
								<?php } ?>
								
							</div>

						</div>
					</div>
					
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Nieuws Categorie&euml;n</h3>
								<ul>
									<li>
										<a href="{url}nieuws">Alle</a>
									</li>
									<li>
										<a href="{url}nieuws?cat=algemee">Algemeen</a>
									</li>
									<li>
										<a href="{url}nieuws?cat=vrouwen">Vrouwen</a>
									</li>
									<li>
										<a href="{url}nieuws?cat=mannen">Mannen</a>
									</li>
									<li>
										<a href="{url}nieuws?cat=jeugd">Jeugd</a>
									</li>
								</ul>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>