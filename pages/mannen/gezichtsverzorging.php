		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Mannen</h1>
                <nav id="primary-menu">
						<ul>
                            <li>
								<a href="{url}mannen/parfum">Parfum</a>
                            </li>
                            <li>
                              <a href="{url}mannen/scheren">Scheren</a>
                            </li>
                            <li class="current">
                              <a href="{url}mannen/gezichtsverzorging">Gezichtsverzorging</a>
                            </li>
                            <li>
                              <a href="{url}mannen/lichaam-douche">Lichaam &amp; Douche</a>
                            </li>
                            <li>
                              <a href="{url}schoonheidsinstituut">Instituut</a>
                            </li>
                        </ul>
                    </nav>
            </div>

        </section>

        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix info-page">
					<div class="row">
						<div class="col-md-6">
							<h3>Gezichtsverzorging</h3>
							<div>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. 
							</div>
							<ul class="iconlist">
								<li><i class="icon-ok"></i> Lijst voorbeeld</li>
								<li><i class="icon-ok"></i> Lijst voorbeeld</li>
								<li><i class="icon-ok"></i> Lijst voorbeeld</li>
								<li><i class="icon-ok"></i> Lijst voorbeeld</li>
								<li><i class="icon-ok"></i> Lijst voorbeeld</li>
							</ul>
						</div>
						<div class="col-md-6 img-holder">
							<img src="{url}images/mozaiek/mannen-01.jpg" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 img-holder">
							<img src="{url}images/mozaiek/mannen-02.jpg" />
						</div>
						<div class="col-md-6">
							<h3>Gezichtsverzorging</h3>
							<div>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. 
							</div>
						</div>
					</div>
					<div class="row">
						<div class="tabs side-tabs clearfix ui-tabs ui-widget ui-widget-content ui-corner-all" id="tab-4">

							<ul class="tab-nav clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
								<li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tabs-13" aria-labelledby="ui-id-29" aria-selected="true"><a href="#tabs-13" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-29">Onderwerp 1</a></li>
								<li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-14" aria-labelledby="ui-id-30" aria-selected="false"><a href="#tabs-14" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-30">Onderwerp 2</a></li>
								<li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-15" aria-labelledby="ui-id-31" aria-selected="false"><a href="#tabs-15" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-31">Onderwerp 3</a></li>
								<li class="hidden-phone ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-16" aria-labelledby="ui-id-32" aria-selected="false"><a href="#tabs-16" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-32">Onderwerp 4</a></li>
							</ul>

							<div class="tab-container">

								<div class="tab-content clearfix ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-13" aria-labelledby="ui-id-29" role="tabpanel" aria-expanded="true" aria-hidden="false">
									<h3>Onderwerp 1</h3>
									Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.
								</div>
								<div class="tab-content clearfix ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-14" aria-labelledby="ui-id-30" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
									<h3>Onderwerp 2</h3>
									Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.
								</div>
								<div class="tab-content clearfix ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-15" aria-labelledby="ui-id-31" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
									<h3>Onderwerp 3</h3>
									<p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
									Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.
								</div>
								<div class="tab-content clearfix ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-16" aria-labelledby="ui-id-32" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
									<h3>Onderwerp 4</h3>
									Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.
								</div>

							</div>

						</div>
					</div>
					
				</div>
				
				<div class="full-width">
				<div class="container clearfix">
					
					<div class="row">
						<div id="oc-images" class="owl-carousel image-carousel">

							<?= Modules::run('brands/brands_logic/index', 'partial'); ?>

						</div>

						<script type="text/javascript">

							jQuery(document).ready(function($) {

								var ocImages = $("#oc-images");

								ocImages.owlCarousel({
									margin: 100,
									nav: true,
									navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
									autoplay: true,
									autoplayHoverPause: true,
									dots: false,
									navRewind: false,
									responsive:{
										0:{ items:2 },
										600:{ items:3 },
										1000:{ items:4 }
									}
								});

							});

						</script>
					</div>
                </div>
				</div>

            </div>

        </section>