		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Schoonheidsinstituut</h1>
            </div>

        </section>
		
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="col_full">
							
							<div class="heading-block center">
								<h4>Epilatie gelaat</h4>
							</div>
							
							<div class="style-msg style-msg-light" style="background-color: #333;">
								<div class="sb-msg">
									<i class="icon-info-sign"></i>Naar gelang uw wensen met pincet of hars.
								</div>
							</div>
							
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Wenkbrauwen € 10,00</h3>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Kaken € 10,00</h3>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Kin € 10,00</h3>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Bovenlip € 10,00</h3>
								</div>
							</div>
							
							<div class="heading-block center" style="margin-top: 50px;">
								<h4>Epilatie Lichaam</h4>
							</div>
							
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Onderbenen € 14,00</h3>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Bovenbenen € 14,00</h3>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Bikini 1 € 15,00</h3>
									<div class="behandeling-info text-left">
										licht bijwerken
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Bikini 2 € 25,00</h3>
									<div class="behandeling-info text-left">
										'String' en 'Bresilian'
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Oksels € 10,00</h3>
								</div>
							</div>
							
							<div class="heading-block center" style="margin-top: 50px;">
								<h4>Epilatie Mannen</h4>
							</div>
							
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Rug € 25,00</h3>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Schouders € 15,00</h3>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Torso € 20,00</h3>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Buik € 18,00</h3>
								</div>
							</div>
							
						</div>	
					</div>
					
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Categorie&euml;n</h3>
								<ul>
									<li>
										<a href="{url}schoonheidsinstituut/gelaatsverzorging">Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/carita-gelaatsverzorging">Carita Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/crystal-clear-gelaatsverzorging">Crystal Clear Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/make-up">Make-up</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/epilatie">Epilatie</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/manicure">Manicure</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/pedicure">Pedicure</a>
									</li>
									
									<li>
										<a href="{url}schoonheidsinstituut/lichaamsverzorging">Lichaamsverzorging</a>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Geel</h3>
								<ul>
									<li>
										Nieuwstraat 68, 2440 Geel
									</li>
									<li>
										+32 (0) 14 58 98 06
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Tessenderlo</h3>
								<ul>
									<li>
										Neerstraat 8, 3980 Tessenderlo
									</li>
									<li>
										+32 (0) 14 67 62 20
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Recente artikels</h3>
								<ul>
									<?= Modules::run('posts/posts_logic/index', 'partial', 5); ?>
								</ul>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>