		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Schoonheidsinstituut</h1>
            </div>

        </section>
		
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="col_full">
							
							<div class="heading-block center">
								<h4>Make-up</h4>
							</div>
							
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Make-up</h3>
									<div class="behandeling-info">
										een dag- of avondmake-up
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 30min</div>
										<div class="col-md-6 text-left">€ 30,00</div>
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Make-up deluxe</h3>
									<div class="behandeling-info">
										een variatie van mogelijkheden: de juiste look aanmeten, kleuradvies of leren maquilleren
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 1u00</div>
										<div class="col-md-6 text-left">€ 45,00</div>
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Bruids Make-up</h3>
									<div class="behandeling-info">
										inclusief proefsessie
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR proef 1u00<br />DUUR trouwdag 30min</div>
										<div class="col-md-6 text-left">€ 75,00</div>
									</div>
								</div>
							</div>
							
						</div>	
					</div>
					
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Categorie&euml;n</h3>
								<ul>
									<li>
										<a href="{url}schoonheidsinstituut/gelaatsverzorging">Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/carita-gelaatsverzorging">Carita Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/crystal-clear-gelaatsverzorging">Crystal Clear Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/make-up">Make-up</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/epilatie">Epilatie</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/manicure">Manicure</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/pedicure">Pedicure</a>
									</li>
									
									<li>
										<a href="{url}schoonheidsinstituut/lichaamsverzorging">Lichaamsverzorging</a>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Geel</h3>
								<ul>
									<li>
										Nieuwstraat 68, 2440 Geel
									</li>
									<li>
										+32 (0) 14 58 98 06
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Tessenderlo</h3>
								<ul>
									<li>
										Neerstraat 8, 3980 Tessenderlo
									</li>
									<li>
										+32 (0) 14 67 62 20
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Recente artikels</h3>
								<ul>
									<?= Modules::run('posts/posts_logic/index', 'partial', 5); ?>
								</ul>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>