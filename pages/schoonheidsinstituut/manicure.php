		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Schoonheidsinstituut</h1>
            </div>

        </section>
		
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="col_full">
							
							<div class="heading-block center">
								<h4>Manicure</h4>
							</div>
							
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Beauty manicure € 25,00</h3>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Crystal Clear Hand Treatment € 25,00</h3>
									<div class="behandeling-info text-left">
										behandeling met kristallen, zonder masker. (DUUR: 10min)
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Surplus lakken € 5,00</h3>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-12">
									<h3 class="text-left">Surplus French lakken € 15,00</h3>
								</div>
							</div>
							
						</div>	
					</div>
					
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Categorie&euml;n</h3>
								<ul>
									<li>
										<a href="{url}schoonheidsinstituut/gelaatsverzorging">Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/carita-gelaatsverzorging">Carita Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/crystal-clear-gelaatsverzorging">Crystal Clear Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/make-up">Make-up</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/epilatie">Epilatie</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/manicure">Manicure</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/pedicure">Pedicure</a>
									</li>
									
									<li>
										<a href="{url}schoonheidsinstituut/lichaamsverzorging">Lichaamsverzorging</a>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Geel</h3>
								<ul>
									<li>
										Nieuwstraat 68, 2440 Geel
									</li>
									<li>
										+32 (0) 14 58 98 06
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Tessenderlo</h3>
								<ul>
									<li>
										Neerstraat 8, 3980 Tessenderlo
									</li>
									<li>
										+32 (0) 14 67 62 20
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Recente artikels</h3>
								<ul>
									<?= Modules::run('posts/posts_logic/index', 'partial', 5); ?>
								</ul>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>