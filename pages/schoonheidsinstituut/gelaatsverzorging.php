		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Schoonheidsinstituut</h1>
            </div>

        </section>
		
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="col_full">
							
							<div class="heading-block center">
								<h4>Gelaatsverzorging</h4>
							</div>
							
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Luxe gelaatsverzorging</h3>
									<div class="behandeling-info">
										een reiniging, peeling, relaxerende gezichtsmassage gevolgd door een masker en afgerond met als extra een geschikte luxe creme van La Prairie of Chanel, inclusief epilerenwenkbrauwen.
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 1u20</div>
										<div class="col-md-6 text-left">€ 65,00</div>
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Basic gelaatsverzorging</h3>
									<div class="behandeling-info">
										een reiniging, peeling, relaxerende gezichtsmassage gevolgd door een masker en afgerond met een geschikte crème, 
										inclusief epileren wenkbrauwen.
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 1u20</div>
										<div class="col-md-6 text-left">€ 45,00</div>
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Mini gelaatsverzorging</h3>
									<div class="behandeling-info">
										een reiniging en peeling gevolgd door een relaxerende gezichtsmassage en afgerond met een geschikte crème.
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 35min</div>
										<div class="col-md-6 text-left">€ 30,00</div>
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Flash gelaatsverzorging</h3>
									<div class="behandeling-info">
										een reiniging en peeling afgerond met  een geschikte crème, inclusief epileren wenkbrauwen.
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 35min</div>
										<div class="col-md-6 text-left">€ 30,00</div>
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Jeugd gelaatsverzorging</h3>
									<div class="behandeling-info">
										geldig tot de leeftijd van 19 jaar, idem als basic gelaatsverzorging met producten geschikt voor jeugd.
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 50min</div>
										<div class="col-md-6 text-left">€ 30,00</div>
									</div>
								</div>
							</div>
							
							<div class="style-msg style-msg-light" style="background-color: #333;">
								<div class="sb-msg">
									<i class="icon-info-sign"></i>Alle gelaatsverzorgingen zijn zowel voor vrouwen en mannen.
								</div>
							</div>
							
						</div>	
					</div>
					
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Categorie&euml;n</h3>
								<ul>
									<li>
										<a href="{url}schoonheidsinstituut/gelaatsverzorging">Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/carita-gelaatsverzorging">Carita Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/crystal-clear-gelaatsverzorging">Crystal Clear Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/make-up">Make-up</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/epilatie">Epilatie</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/manicure">Manicure</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/pedicure">Pedicure</a>
									</li>
									
									<li>
										<a href="{url}schoonheidsinstituut/lichaamsverzorging">Lichaamsverzorging</a>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Geel</h3>
								<ul>
									<li>
										Nieuwstraat 68, 2440 Geel
									</li>
									<li>
										+32 (0) 14 58 98 06
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Tessenderlo</h3>
								<ul>
									<li>
										Neerstraat 8, 3980 Tessenderlo
									</li>
									<li>
										+32 (0) 14 67 62 20
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Recente artikels</h3>
								<ul>
									<?= Modules::run('posts/posts_logic/index', 'partial', 5); ?>
								</ul>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>