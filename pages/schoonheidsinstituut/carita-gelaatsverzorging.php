		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Schoonheidsinstituut</h1>
            </div>

        </section>
		
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="col_full">
							
							<div class="heading-block center">
								<h4>Carita Gelaatsverzorging</h4>
							</div>
							
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Carita Cinetic lift expert</h3>
									<div class="behandeling-info">
										<u>FASE 1 RELAXATIE</u><br />
										laat u omgeven door de ervaren handen van uw schoonheidsspecialiste om dagelijkse spanningen weg te nemen met een ontspannende massage.
										<br /><br />
										<u>FASE 2 RENOVATEUR</u><br />
										mengt zonnebloempitten en essentiële oliën om de huidkorrel te zuiveren, te verzachten, te exfoliëren en om een unieke glans te onthullen.
										<br /><br />
										<u>FASE 3 CINETIC LIFT EXPERT</u><br />
										een goed presterend apparaat bestemd om de werking van de producten te optimaliseren en het manuele gebaar te voltooien. Hij reinigt de huid in de diepte, corrigeert de onzuiverheden, bevordert de penetratie van de bestanddelen, stimuleert en tonifieert de weefsels op een zichtbare en onmiddellijke manier, de huid is ‘gelift’, verstevigt, stralend en zorgt voor een frisse teint.
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 1u20</div>
										<div class="col-md-6 text-left">€ 95,00</div>
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Carita cinetic renovateur</h3>
									<div class="behandeling-info">
										<u>FASE 1</u><br />
										introductie massage.
										<br /><br />
										<u>FASE 2</u><br />
										renovateur gevolgd door de skin scrubber, een relaxmassage, een masker en afgerond met een geschikte crème. inclusief epileren wenkbrauwen.
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 1u20</div>
										<div class="col-md-6 text-left">€ 65,00</div>
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Carita cinetic lift expert lighttherapie</h3>
									<div class="behandeling-info">
										<u>FASE 1</u><br />
										introductie massage.
										<br /><br />
										<u>FASE 2</u><br />
										renovateur gevolgd door de skin scrubber, een relaxmassage, een masker.
										<br /><br />
										<u>FASE 3</u><br />
										het cinetic lift expert apparaat, de lichttherapie met aangepaste kleur voor uw huid  en afgerond met een geschikte crème. inclusief epileren wenkbrauwen.
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 1u50</div>
										<div class="col-md-6 text-left">€ 120,00</div>
									</div>
								</div>
							</div>
							<div class="behandeling">
								<div class="col-md-4">
									<img src="{url}images/behandeling.jpg" />
								</div>
								<div class="col-md-8">
									<h3>Carita mask lissant repulpant</h3>
									<div class="behandeling-info">
										als optie bij een gelaatsverzorging van Carita, dit professioneel anti-rimpel masker geeft samen met de techniek van Carita volume en ‘lifting’.
									</div>
									<div class="behandeling-details">
										<div class="col-md-6 text-right">DUUR 20min</div>
										<div class="col-md-6 text-left">€ 35,00</div>
									</div>
								</div>
							</div>
							
						</div>	
					</div>
					
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Categorie&euml;n</h3>
								<ul>
									<li>
										<a href="{url}schoonheidsinstituut/gelaatsverzorging">Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/carita-gelaatsverzorging">Carita Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/crystal-clear-gelaatsverzorging">Crystal Clear Gelaatsverzorging</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/make-up">Make-up</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/epilatie">Epilatie</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/manicure">Manicure</a>
									</li>
									<li>
										<a href="{url}schoonheidsinstituut/pedicure">Pedicure</a>
									</li>
									
									<li>
										<a href="{url}schoonheidsinstituut/lichaamsverzorging">Lichaamsverzorging</a>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Geel</h3>
								<ul>
									<li>
										Nieuwstraat 68, 2440 Geel
									</li>
									<li>
										+32 (0) 14 58 98 06
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Tessenderlo</h3>
								<ul>
									<li>
										Neerstraat 8, 3980 Tessenderlo
									</li>
									<li>
										+32 (0) 14 67 62 20
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Recente artikels</h3>
								<ul>
									<?= Modules::run('posts/posts_logic/index', 'partial', 5); ?>
								</ul>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>