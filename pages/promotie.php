		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Promoties</h1>
            </div>

        </section>

        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="pricing-box pricing-extended bottommargin clearfix">

							<div class="pricing-desc" style="width: 100%;">
								<div class="pricing-title">
									<h3>Gratis staaltjes van Chanel Nr 5</h3>
								</div>
								<div class="pricing-features">
									<ul class="iconlist-color clearfix">
										<li><i class="icon-info"></i> Af te halen bij Duvem Geel</li>
										<li><i class="icon-clock"></i> Tot 1 September 2015</li>
									</ul>
									<div style="margin-top: 20px;">
										Reserveer een staaltje van Chanel Nr 5. Gratis af te halen bij Duvem Geel. Haast je want er zijn er maar 10 beschikbaar...
									</div>
								</div>
							</div>

							<div class="pricing-action-area" style="width: 100%; position: relative; height: auto; float: left; border-left: none; border-top: 1px solid rgba(0,0,0,0.05);">
								<input type="text" id="template-contactform-name" name="template-contactform-name" value="" placeholder="Vul hier uw e-mailadres in..." class="sm-form-control required" style="margin-bottom: 20px;" aria-required="true">
								<div class="pricing-action">
									<a href="#" class="button button-3d button-large btn-block nomargin">Verstuur</a>
								</div>
							</div>

						</div>
					</div>
					<!-- SIDEBAR COLUMN -->
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Geel</h3>
								<ul>
									<li>
										Nieuwstraat 68, 2440 Geel
									</li>
									<li>
										+32 (0) 14 58 98 06
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
								<h3>Openingsuren</h3>
								<ul>
									<li class="openhours">
										Maandag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Dinsdag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Woensdag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Donderdag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Vrijdag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Zaterdag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Zondag<span>Gesloten</span>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Tessenderlo</h3>
								<ul>
									<li>
										Neerstraat 8, 3980 Tessenderlo
									</li>
									<li>
										+32 (0) 14 67 62 20
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
								<h3>Openingsuren</h3>
								<ul>
									<li class="openhours">
										Maandag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Dinsdag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Woensdag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Donderdag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Vrijdag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Zaterdag<span>10u00 - 18u00</span>
									</li>
									<li class="openhours">
										Zondag<span>Gesloten</span>
									</li>
								</ul>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>