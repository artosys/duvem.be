		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Contact</h1>
            </div>

        </section>

		<section id="google-map" class="gmap slider-parallax"></section>

		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="js/jquery.gmap.js"></script>

		<script type="text/javascript">

			$('#google-map').gMap({

				address: 'Nieuwstraat 68 Geel, Belgium',
				maptype: 'ROADMAP',
				zoom: 15,
				markers: [
					{
						address: "Nieuwstraat 68 Geel, Belgium",
						html: '<div style="width: 300px;"><h4 style="margin-bottom: 8px;">Hi, we\'re <span>Envato</span></h4><p class="nobottommargin">Our mission is to help people to <strong>earn</strong> and to <strong>learn</strong> online. We operate <strong>marketplaces</strong> where hundreds of thousands of people buy and sell digital goods every day, and a network of educational blogs where millions learn <strong>creative skills</strong>.</p></div>',
						icon: {
							image: "images/icons/map-icon-red.png",
							iconsize: [32, 39],
							iconanchor: [13,39]
						}
					}
				],
				doubleclickzoom: false,
				controls: {
					panControl: true,
					zoomControl: true,
					mapTypeControl: true,
					scaleControl: false,
					streetViewControl: false,
					overviewMapControl: false
				}

			});

		</script>
		
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="postcontent nobottommargin">

							<h3>Stuur ons een bericht</h3>

							<div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>

							<form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post" novalidate="novalidate">

								<div class="form-process"></div>

								<div class="col_one_third">
									<label for="template-contactform-name">Naam <small>*</small></label>
									<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" aria-required="true">
								</div>

								<div class="col_one_third">
									<label for="template-contactform-email">E-mail <small>*</small></label>
									<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" aria-required="true">
								</div>

								<div class="col_one_third col_last">
									<label for="template-contactform-phone">Telefoonnummer</label>
									<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control">
								</div>

								<div class="clear"></div>

								<div class="col_two_third">
									<label for="template-contactform-subject">Onderwerp <small>*</small></label>
									<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" aria-required="true">
								</div>

								<div class="col_one_third col_last">
									<label for="template-contactform-service">Type</label>
									<select id="template-contactform-service" name="template-contactform-service" class="sm-form-control">
										<option value="">-- Kies een type bericht --</option>
										<option value="Wordpress">Infomatie</option>
										<option value="PHP / MySQL">Reserveer</option>
									</select>
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<label for="template-contactform-message">Bericht <small>*</small></label>
									<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30" aria-required="true"></textarea>
								</div>

								<div class="col_full hidden">
									<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control">
								</div>

								<div class="col_full">
									<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Verstuur bericht</button>
								</div>

							</form>

							<script type="text/javascript">

								$("#template-contactform").validate({
									submitHandler: function(form) {
										$('.form-process').fadeIn();
										$(form).ajaxSubmit({
											target: '#contact-form-result',
											success: function() {
												$('.form-process').fadeOut();
												$(form).find('.sm-form-control').val('');
												$('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
												SEMICOLON.widget.notifications($('#contact-form-result'));
											}
										});
									}
								});

							</script>

						</div>
					</div>
					<!-- SIDEBAR COLUMN -->
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Geel</h3>
								<ul>
									<li>
										Nieuwstraat 68, 2440 Geel
									</li>
									<li>
										+32 (0) 14 58 98 06
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
								<h3>Openingsuren</h3>
								<?= Modules::run('openhours/openhours_logic/show_hours', 'partial'); ?>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Tessenderlo</h3>
								<ul>
									<li>
										Neerstraat 8, 3980 Tessenderlo
									</li>
									<li>
										+32 (0) 14 67 62 20
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
								<h3>Openingsuren</h3>
								<?= Modules::run('openhours/openhours_logic/show_hours', 'partial'); ?>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>