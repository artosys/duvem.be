		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Promoties</h1>
            </div>

        </section>

        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="pricing-box pricing-extended bottommargin clearfix">

							<div class="pricing-desc">
								<div class="pricing-title">
									<h3>Gratis staaltjes van Chanel Nr 5</h3>
								</div>
								<div class="pricing-features">
									<ul class="iconlist-color clearfix">
										<li><i class="icon-info"></i> Af te halen bij Duvem Geel</li>
										<li><i class="icon-clock"></i> Tot 1 September 2015</li>
									</ul>
									<div style="margin-top: 20px;">
										Reserveer een staaltje van Chanel Nr 5. Gratis af te halen bij Duvem Geel. Haast je want er zijn er maar 10 beschikbaar...
									</div>
								</div>
							</div>

							<div class="pricing-action-area">
								<div class="pricing-meta">
									Nog
								</div>
								<div class="pricing-price">
									8 / 10<span class="price-tenure">Beschikbaar</span>
								</div>
								<div class="pricing-action">
									<a href="{url}promotie" class="button button-3d button-large btn-block nomargin">Reserveer</a>
								</div>
							</div>

						</div>
					</div>
					<!-- SIDEBAR COLUMN -->
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Geel</h3>
								<ul>
									<li>
										Nieuwstraat 68, 2440 Geel
									</li>
									<li>
										+32 (0) 14 58 98 06
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
								<h3>Openingsuren</h3>
								<?= Modules::run('openhours/openhours_logic/show_hours', 'partial'); ?>
							</div>
						</div>
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Duvem Tessenderlo</h3>
								<ul>
									<li>
										Neerstraat 8, 3980 Tessenderlo
									</li>
									<li>
										+32 (0) 14 67 62 20
									</li>
									<li>
										info@duvem.be
									</li>
								</ul>
								<h3>Openingsuren</h3>
								<?= Modules::run('openhours/openhours_logic/show_hours', 'partial'); ?>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>