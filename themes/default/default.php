<?php 
	$title = "";
	$home_layout = FALSE;
	$body_class = '';
	if ($this->config->item('sys_active_page') == 'pages/home') { $title = " - Home"; $home_layout = TRUE; }
	elseif ($this->config->item('sys_active_page') == 'pages/vrouwen/start') { $title = " - Vrouwen"; }
	elseif ($this->config->item('sys_active_page') == 'pages/mannen/start') { $title = " - Mannen"; }
	elseif ($this->config->item('sys_active_page') == 'pages/jeugd/start') { $title = " - Jeugd"; }
	
	if (strpos($this->config->item('sys_active_page'),'mannen') !== false) { 
		if ( $this->config->item('sys_active_page') == 'pages/mannen/start' ) { $body_class = "mannen"; }
		else { $body_class = "mannen no-bg-img"; }
	}
	elseif (strpos($this->config->item('sys_active_page'),'jeugd') !== false) { $body_class = "jeugd"; }
	
	$description = "Duvem heeft 2 vestigingen, in Geel en Tessenderlo. Je kan er terecht voor parfums, make-up, verzorgingsproducten en lichaamsbehandelingen.";
	if($this->config->item('article_description'))
	{
		$description = $this->config->item('article_description');
	}
?>
<!DOCTYPE html>
<html dir="ltr" lang="nl-NL">
	<head>
		<meta charset="utf-8">
		<title>Duvem Parfumerie<?= $title ?></title>
		<meta name="description" content="<?= $description ?>">
		<meta name="keywords" content="Duvem, Parfumerie, Geel, Make-up, Verzorgingsproducten, Gelaatsbehandelingen, Manicure, Pedicure, Tessenderlo, Nieuwstraat, Mannen, Vrouwen, Jeugd, Schoonheidsinstituut" />
		<meta name="author" content="Cubro bvba">
		<meta name="robots" content="index, follow" />
		<meta name="revisit-after" content="3 days" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		
		<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{url}css/bootstrap.css" type="text/css" />
		<link rel="stylesheet" href="{url}css/style.css" type="text/css" />
		<link rel="stylesheet" href="{url}css/dark.css" type="text/css" />
		<link rel="stylesheet" href="{url}css/font-icons.css" type="text/css" />
		<link rel="stylesheet" href="{url}css/animate.css" type="text/css" />
		<link rel="stylesheet" href="{url}css/magnific-popup.css" type="text/css" />

		<link rel="stylesheet" href="{url}css/responsive.css" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="{url}images/favicon.png">
		
		<?php $this->layout->partial("platforms/pharma/views/manager_head", TRUE, $content_data); ?>
		<?php $this->layout->partial("platforms/black/views/manager_head", TRUE, $content_data); ?>
		
		<script type="text/javascript" src="<?= SHAREURL ?>js/jquery.1.11.1.js"></script>
		<script type="text/javascript" src="<?= SHAREURL ?>js/bootstrap.3.3.1.js"></script>
		<script type="text/javascript" src="{url}js/plugins.js"></script>
	</head>
	
<body class="stretched <?= $body_class ?>">
	
	<?php $this->layout->partial("platforms/pharma/views/manager_bar", TRUE, $content_data); ?>
	
	<div id="wrapper" class="clearfix">
		
		<?php if ( $home_layout ) { ?>
		<section id="slider" class="slider-parallax full-screen with-header force-full-screen clearfix">

            <div class="full-screen force-full-screen bg-white">

                <div class="container clearfix">
                    <div class="emphasis-title vertical-middle center">
                        <h1 data-animate="fadeInUp"><strong>Duvem </strong>Parfumerie</h1>
                    </div>
					
					<div class="start-image"></div>
                </div>

            </div>

        </section>
		<?php } ?>
		
		<header id="header" class="<?php $home_layout ? 'transparent-header':'full-header'; ?>">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <div id="logo">
						<?php if ( $body_class == 'mannen' || $body_class == 'mannen no-bg-img' ) { ?>
						<a href="index.html" class="standard-logo" data-dark-logo="{url}images/logo_duvem_white.png"><img src="{url}images/logo_duvem_white.png" alt="Duvem Logo"></a>
                        <a href="index.html" class="retina-logo" data-dark-logo="{url}images/logo_duvem_white.png@2x.png"><img src="{url}images/logo_duvem_white.png2x.png" alt="Duvem Logo"></a>
						<?php } else { ?>
                        <a href="{url}" class="standard-logo" data-dark-logo="{url}images/logo_duvem.png"><img src="{url}images/logo_duvem.png" alt="Duvem Logo"></a>
                        <a href="{url}" class="retina-logo" data-dark-logo="{url}images/logo_duvem@2x.png"><img src="{url}images/logo_duvem@2x.png" alt="Duvem Logo"></a>
						<?php } ?>
                    </div>
					
					<?php $active = $this->config->item('sys_active_page'); ?>
                    <nav id="primary-menu">
						<ul>
                            <li><a href="{url}">Home</a>
                            </li>
                            <li <?php if ( strpos($this->config->item('sys_active_page'),'vrouwen') !== false ) { echo 'class="current"'; } ?>>
                              <a href="{url}vrouwen/start">Vrouwen</a>
                            </li>
                            <li <?php if ( strpos($this->config->item('sys_active_page'),'mannen') !== false ) { echo 'class="current"'; } ?>>
                              <a href="{url}mannen/start">Mannen</a>
                            </li>
                            <li <?php if ( strpos($this->config->item('sys_active_page'),'jeugd') !== false ) { echo 'class="current"'; } ?>>
                              <a href="{url}jeugd/start">Jeugd</a>
                            </li>
                            <li <?php if ( $active == 'pages/nieuws' ) { echo 'class="current"'; } ?>>
                              <a href="{url}nieuws">Nieuws</a>
                            </li>
                            <li <?php if ( strpos($this->config->item('sys_active_page'),'schoonheidsinstituut') !== false ) { echo 'class="current"'; } ?>>
                              <a href="{url}schoonheidsinstituut/start">Schoonheidinstituut</a>
                            </li>
                            <li <?php if ( $active == 'pages/promoties' || $active == 'pages/promotie' ) { echo 'class="current"'; } ?>>
                              <a href="{url}promoties">Promoties</a>
                            </li>
                            <li <?php if ( $active == 'pages/contact' ) { echo 'class="current"'; } ?>>
                              <a href="{url}contact">Contact</a>
                            </li>
                        </ul>
                    </nav>

                </div>

            </div>

        </header>
		
		<?php $this->layout->partial($layout_content, TRUE, $content_data); ?>	
		
		<?php if ( $home_layout ) { ?>
		<div class="clear"></div>
		<?php } else { ?>
		<footer id="footer" class="dark">

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col-md-4" style="line-height: 34px;">
                        Copyrights &copy; 2015 <a href="{url}login/">Duvem</a>
                    </div>

                    <div class="col-md-8">
						
						<div class="col-md-6" style="line-height: 34px;">
									<a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
									Volg ons op Facebook
						</div>
						
						<div class="col-md-6">
							<?= Modules::run('newsletter/newsletter_logic/show_form', 'partial', '1'); ?>
							
							
						</div>
                    </div>

                </div>

            </div>

        </footer>
		<?php } ?>
		
	</div>
	
	<div id="gotoTop" class="icon-angle-up"></div>
		
		<script type="text/javascript">
			var siteurl = "{url}";
		</script>
		<?php $this->layout->partial("platforms/pharma/views/manager_footer", TRUE, $content_data); ?>
		<script type="text/javascript" src="{url}js/functions.js"></script>
		<script type="text/javascript" src="{url}js/shop.js"></script>

</body>
</html>