
	<form action="" name="form_newsletter" method="post" id="subscribe" class="nobottommargin" enctype="multipart/form-data">
		<?= validation_success() ?>
		<?php echo validation_errors(); ?>
		<div class="input-group divcenter">
		<?php if(in_array('email', $included)){ ?>
				<span class="input-group-addon"><i class="icon-email2"></i></span>
                <input type="text" id="email" name="email" class="form-control required email" value="{email}" placeholder="Uw E-mail">
		<?php } ?>
		
		<?php if(in_array('name', $included)){ ?>
		<div class="form_row">
			<label for="name">Naam</label>
			<div class="input">
				<input type="text" value="{name}" placeholder="Naam" id="name" name="name">
			</div>
		</div>
		<?php } ?>
		
		<?php if(in_array('city', $included)){ ?>
		<div class="form_row">
			<label for="city">Gemeente</label>
			<div class="input">
				<input type="text" value="{city}" placeholder="Gemeente" id="city" name="city">
			</div>
		</div>
		<?php } ?>
		
		<?php if(in_array('phone', $included)){ ?>
		<div class="form_row">
			<label for="phone">Telefoonnummer</label>
			<div class="input">
				<input type="text" value="{phone}" placeholder="Telefoonnummer" id="phone" name="phone">
			</div>
		</div>
		<?php } ?>
		
		<span class="input-group-btn">
			<input type="submit" name="btn_submit" class="btn btn-success" id="submit" value="Inschrijven" />
        </span>
	</div>
	</form>
