	<ul>
				<?php 
				$names = ($settings->short_names == 1) ? $this->config->item('cal_days_short') : $this->config->item('cal_days');
				for($i = 0; $i < 7; $i++)
				{
					echo '<li class="openhours">'.$names[$i].'<span>';

					if(isset($openhours[$i+1]))
					{
						$start1 = strtotime($openhours[$i+1]->start_1);
						$start2 = strtotime($openhours[$i+1]->start_2);
						$end1   = strtotime($openhours[$i+1]->end_1);
						$end2   = strtotime($openhours[$i+1]->end_2);

						if($start2 == $end1)
						{
							echo date("H\ui",$start1).' - '.date("H\ui",$end2);
						}
						else
						{
							echo date("H\ui",$start1).' - '.date("H\ui",$end1);
							echo ' en '.date("H\ui",$start2).' - '.date("H\ui",$end2);
						}
					}
					else
					{
						echo 'Gesloten';
					}
					echo '</span></li>';
				}
				?>
	</ul>