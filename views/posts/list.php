<?php if ($this->config->item('sys_active_page') == 'pages/nieuws') { $aantal_posts = count($posts); ?>
	
	<?php if ( count($posts) == 0 ) { echo '<h4>Er zijn nog geen nieuwsartikels binnen deze categorie.</h4>'; } else { ?>
	<?php foreach($posts as $post){ ?>

								<?php 
									setlocale(LC_TIME, 'nl_NL');	
									$datetime = new DateTime($post->date_created);
								?>
						
								<div class="entry clearfix">
									<div class="entry-timeline">
										<?= $datetime->format('d') ?><span><?= substr($datetime->format('F'), 0, 3) ?></span>
										<div class="timeline-divider"></div>
									</div>
									<?php if ( count($post->pictures) > 0 ) { ?>
									<div class="entry-image">
										<a href="images/blog/full/17.jpg" data-lightbox="image">
											<img class="image_fade" alt="" src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$post->pictures[0]->src_original ?>" alt="<?= $post->title ?>" style="opacity: 1;">
										</a>
									</div>
									<?php } ?>
									<div class="entry-title">
										<h2><a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>"><?= $post->title ?></a></h2>
									</div>
									<ul class="entry-meta clearfix">
										<li><i class="icon-folder-open"></i> <a href="#"><?= $post->category ?></a></li>
										<li><a href="{url}posts/like/<?= $post->id ?>"><i class="icon-thumbs-up2"></i> <?= $post->likes ?> Likes</a></li>
										<li><a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments"><i class="icon-comments"></i> <?= $post->comments ?> Comments</a></li>
										<li><a href="javascript:fbShare('{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>', 520, 350)"><i class="icon-share-alt"></i> <?= $post->shared ?> Shares</a></li>
									</ul>
									<div class="entry-content">
										<div style="max-height: 100px; overflow:hidden;"><?= nl2br(substr($post->text, 0, 330)) ?></div>
										<ul class="social-box">
											<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>">
												<li>
													<span><i class="icon-eye-open"></i>Meer lezen</span>
												</li>
											</a>
											<a href="{url}posts/like/<?= $post->id ?>">
												<li>
													<span><i class="icon-thumbs-up2"></i>Vind ik leuk</span>
												</li>
											</a>
											<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments">
												<li>
													<span><i class="icon-comments"></i>Reageren</span>
												</li>
											</a>
											<a href="javascript:fbShare('{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>', 520, 350)">
												<li>
													<span><i class="icon-facebook"></i>Delen</span>
												</li>
											</a>
										</ul>
									</div>
								</div>
		
	<?php } ?>
	<?php } ?>

<?php } else { ?>
	
	<?php if ( count($posts) == 0 ) { echo '<li style="border-bottom: none; text-align: center; color: black;">Er zijn nog geen artikels</li>'; } else { ?>

		<?php foreach($posts as $post){ ?>

										<li>
											<div class="col-md-2">
											<?php if ( count($post->pictures) > 0 ) { ?>
												<img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$post->pictures[0]->src_original ?>" alt="<?= $post->title ?>" />
											<?php } ?>
											</div>
											<div class="col-md-10">
												<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>">
													<?= $post->title ?>
												</a><br />
												<span>
																<?php 
																setlocale(LC_TIME, 'nl_NL');	
																$datetime = new DateTime($post->date_created);
																	echo $datetime->format('d').' '.$datetime->format('F').' '.$datetime->format('Y'); 
																?>
												</span>
											</div>
										</li>

		<?php } ?>
	<?php } ?>

<?php } ?>

<script type="text/javascript">
	function fbShare(url, winWidth, winHeight) {
        var winTop = (screen.height / 2) - (winHeight / 2);
        var winLeft = (screen.width / 2) - (winWidth / 2);
        window.open('http://www.facebook.com/sharer.php?s=100&p[url]=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    }
</script>