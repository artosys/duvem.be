		<section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>Nieuws</h1>
                <nav id="primary-menu">
						<ul>
                            <li <?php if ( isset($_GET['cat']) && $_GET['cat'] == 'algemee' ) { echo 'class="current"'; } ?>>
								<a href="{url}nieuws?cat=algemee">Algemeen</a>
                            </li>
							<li <?php if ( isset($_GET['cat']) && $_GET['cat'] == 'vrouwen' ) { echo 'class="current"'; } ?>>
								<a href="{url}nieuws?cat=vrouwen">Vrouwen</a>
                            </li>
                            <li <?php if ( isset($_GET['cat']) && $_GET['cat'] == 'mannen' ) { echo 'class="current"'; } ?>>
                              <a href="{url}nieuws?cat=mannen">Mannen</a>
                            </li>
                            <li <?php if ( isset($_GET['cat']) && $_GET['cat'] == 'jeugd' ) { echo 'class="current"'; } ?>>
                              <a href="{url}nieuws?cat=jeugd">Jeugd</a>
                            </li>
                        </ul>
                    </nav>
            </div>

        </section>

		<section id="content">

            <div class="content-wrap">

                <div class="container clearfix">
					
					<div class="col-md-8">
						<div class="postcontent nobottommargin clearfix">
						
							<div id="posts" class="post-timeline clearfix">

								<?php 
									setlocale(LC_TIME, 'nl_NL');	
									$datetime = new DateTime($post->date_created);
								?>
						
								<div class="entry clearfix" style="border-bottom: none; padding-bottom: 0;">
									<div class="entry-timeline">
										<?= $datetime->format('d') ?><span><?= substr($datetime->format('F'), 0, 3) ?></span>
										<div class="timeline-divider"></div>
									</div>
									<?php if ( count($post->pictures) > 0 ) { ?>
									<div class="entry-image">
										<a href="images/blog/full/17.jpg" data-lightbox="image">
											<img class="image_fade" alt="" src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$post->pictures[0]->src_original ?>" alt="<?= $post->title ?>" style="opacity: 1;">
										</a>
									</div>
									<?php } ?>
									<div class="entry-title">
										<h2><?= $post->title ?></h2>
									</div>
									<ul class="entry-meta clearfix">
										<li><a href="{url}posts/like/<?= $post->id ?>"><i class="icon-thumbs-up2"></i> <?= count($likes) ?> Likes</a></li>
										<li><a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments"><i class="icon-comments"></i> <?= count($comments) ?> Comments</a></li>
										<li><a href="javascript:fbShare('{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>', 520, 350)"><i class="icon-share-alt"></i> <?= $post->shared ?> Shares</a></li>
									</ul>
									<div class="entry-content">
										<div style="overflow:hidden;"><?= nl2br($post->text) ?></div>
										<ul class="social-box">
											<a href="{url}posts/like/<?= $post->id ?>">
												<li>
													<span><i class="icon-thumbs-up2"></i>Vind ik leuk</span>
												</li>
											</a>
											<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments">
												<li>
													<span><i class="icon-comments"></i>Reageren</span>
												</li>
											</a>
											<a href="javascript:fbShare('{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>', 520, 350)">
												<li>
													<span><i class="icon-facebook"></i>Delen</span>
												</li>
											</a>
										</ul>
									</div>
								</div>
								
							</div>
							
							<?php if($post->allow_comments == 1){ ?>
							<div id="comments" class="clearfix" style="margin-top: 0;">
								
								<?php if(count($comments) > 0){ ?>
                                <h3 id="comments-title"><span><?= count($comments) ?></span> Reactie<?= count($comments) == 1 ? "":"s" ?></h3>

                                <ol class="commentlist clearfix">
									
									<?php foreach($comments as $comment){ ?>
                                    <li class="comment byuser comment-author-_smcl_admin even thread-odd thread-alt depth-1" id="li-comment-2">

                                        <div id="comment-2" class="comment-wrap clearfix">

                                            <div class="comment-meta">

                                                <div class="comment-author vcard">

                                                    <span class="comment-avatar clearfix">
                                                    <img alt='' src='http://1.gravatar.com/avatar/30110f1f3a4238c619bcceb10f4c4484?s=60&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D60&amp;r=G' class='avatar avatar-60 photo' height='60' width='60' /></span>

                                                </div>

                                            </div>

                                            <div class="comment-content clearfix">

                                                <div class="comment-author"><?= $comment->name ?><span><?= strftime("%d %B, %Y", strtotime($comment->date_created)) ?></span></div>

												<div style="margin-top: 20px;"><?= nl2br($comment->comment) ?></div>

                                            </div>

                                            <div class="clear"></div>

                                        </div>

                                    </li>
									<?php } ?>

                                </ol>

                                <div class="clear"></div>
								<?php } ?>
								
								<form method="post" action="{url}posts/comment/<?= $post->id ?>" id="post_comment">
                                <div id="respond" class="clearfix">

                                    <h3>Reactie <span>plaatsen</span></h3>

                                        <div class="col_one_third">
                                            <label for="name">Uw naam</label>
                                            <input type="text" name="name" id="comment_name" tabindex="1" class="sm-form-control" >
                                        </div>

                                        <div class="col_one_third">
                                            <label for="email">Uw e-mail</label>
                                            <input type="email" name="email" id="comment_email" tabindex="2" class="sm-form-control" >
                                        </div>

                                        <div class="clear"></div>

                                        <div class="col_full">
                                            <label for="comment">Uw bericht</label>
                                            <textarea name="comment" id="comment_comment" cols="58" rows="7" tabindex="3" class="sm-form-control"></textarea>
                                        </div>

                                        <div class="col_full nobottommargin">
                                            <input type="submit" name="btn_comment" value="Reactie toevoegen">
                                        </div>

                                </div>
								</form>

                            </div>
							<?php } ?>

						</div>
					</div>
					
					<div class="col-md-4">
						
						<div class="side-box">
							<div class="side-box-inner">
								<h3>Nieuws Categorie&euml;n</h3>
								<ul>
									<li>
										<a href="{url}nieuws">Alle</a>
									</li>
									<li>
										<a href="{url}nieuws?cat=algemee">Algemeen</a>
									</li>
									<li>
										<a href="{url}nieuws?cat=vrouwen">Vrouwen</a>
									</li>
									<li>
										<a href="{url}nieuws?cat=mannen">Mannen</a>
									</li>
									<li>
										<a href="{url}nieuws?cat=jeugd">Jeugd</a>
									</li>
								</ul>
							</div>
						</div>
						
					</div>
                </div>

            </div>

        </section>

		<script type="text/javascript">
			$(document).ready(function(){
				$("#post_comment").submit(function(e) {
					comment_valid = true;
					$("#comment_error").text('');
					if($.trim($("#comment_name").val()) == "")
					{
						comment_valid = false;
					}
					var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
					if($.trim($("#comment_email").val()) == "" || !re.test($("#comment_email").val()))
					{
						comment_valid = false;
					}
					if($.trim($("#comment_comment").val()) == "")
					{
						comment_valid = false;
					}
					if(comment_valid == false)
					{
						e.preventDefault();
						$("#comment_error").text('Gelieve alle velden correct en volledig in te vullen.');
						$("#comment_error").show();
					}
				});
			});
			function fbShare(url, winWidth, winHeight) {
				var winTop = (screen.height / 2) - (winHeight / 2);
				var winLeft = (screen.width / 2) - (winWidth / 2);
				window.open('http://www.facebook.com/sharer.php?s=100&p[url]=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
			}
		</script>